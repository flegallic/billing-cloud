## Flexible Engine billing reports

### Authentication
You must create a key to call Arrow API : https://developer.orange.com/myapps \
The examples in this section use the (non-working) credentials in the following table.
<table>
  <tr>
    <th>Parameter</th>
    <th>Value</th>
  </tr>
  <tr>
    <td>orange_token</td>
    <td>feEXAMPLE</td>
  </tr>
  <tr>
    <td>cloudstore_token</td>
    <td>EYSEXvsjFcEXAMPLEKEY</td>
  </tr>
  <tr>
    <td>Contract ID</td>
    <td>xxxxxxxxxxxxxxxxxx</td>
  </tr>
</table>

### python script example
minimum version : Python 3.6.x
```python
# -*- coding: utf-8 -*-
import requests,json,os,urllib3,datetime
from datetime import datetime,date
from dateutil.relativedelta import relativedelta
from openpyxl import load_workbook
urllib3.disable_warnings()
```

### VAULT authentication methods (without AppRole)
- Variables should be adapted for your specific context
```python
vault_addr    = "your_vault_secret_engine"
today         = date.today()
endDate       = date(today.year, today.month, 1)
startDate     = endDate + relativedelta(months=-1)
fe_month      = startDate.strftime("%Y%m")
BillingPeriod = startDate.strftime("%Y-%m-28")
```

```python
def get_vault_credentials(vault_addr,vault_headers):
    r = requests.get(vault_addr, headers=vault_headers, verify=False)
    if r.status_code == 200:
        vault_result = r.json()
        return vault_result
    else:
        print(r.status_code)
        
vault_login          = input("Please enter your cuid (AD-SUBS) : ")
vault_token          = os.popen(f"vault login -method=ldap -path=ad-subs username={vault_login}").read()
print(vault_token)
token                = input("Please enter your vault token : ")
vault_headers        = {"X-Vault-Token": f"{token}","Content-Type": "application/json"}
vault_addr           = "your_vault_secret_engine"
vault_result         = get_vault_credentials(vault_addr,vault_headers)
fe_autorizationId    = vault_result['data']['Authorization header']
fe_cloudstore_token  = vault_result['data']['Cloudstore token']
fe_contract_id       = vault_result['data']['Contract ID']
```

### Get fe url + credentials
```python
fe_token_url         = "https://api.orange.com/oauth/v2/token"
fe_data              = {"grant_type": "client_credentials"}
fe_headers           = {"Accept": "application/json","Authorization": f"{fe_autorizationId}"}
fe_customers_url     = "https://api.orange.com/cloud/b2b/v1/contracts"
fe_contract_url      = "https://api.orange.com/cloud/b2b/v1/contract"
fe_bills_url         = "https://api.orange.com/cloud/b2b/v1/documents?documentType=bills"
```

### Get Customers
```python
def get_csp_customers(fe_token_url,fe_orange_token,fe_cloudstore_token,fe_contract_id,fe_data,fe_headers,fe_customers_url,fe_contract_url):
    print('Please wait a moment...(this can again take several minutes)')

    r = requests.post(fe_token_url, data=fe_data, headers=fe_headers)
    if r.status_code == 200:
        token = r.json()
        fe_token_access = token['access_token']
        contracts_headers  = {
            "Accept": "application/json",
            "Authorization": f"Bearer {fe_orange_token}",
            "X-API-Key": f"{fe_cloudstore_token}"
        }
        r = requests.get(fe_customers_url, headers=contracts_headers)
        if r.status_code == 200:
            nb_fe_customers = r.json()
            for fe_customers in nb_fe_customers:
                Reference   = fe_customers['id']
                TenantName  = fe_customers['name']
                Created     = fe_customers['createdAt']
                Tag         = (fe_customers['offer']['name']).lower()
                contract_headers  = {
                    "Accept": "application/json",
                    "Authorization": f"Bearer {fe_orange_token}",
                    "X-API-Key": f"{fe_cloudstore_token}",
                    "X-ECCS-Contract-Id": f"{Reference}"
                }
                r = requests.get(fe_contract_url, headers=contract_headers)
                nb_fe_contracts = r.json()      
                LastName   = (nb_fe_contracts['contact']['lastName']).upper()
                FirstName  = (nb_fe_contracts['contact']['firstName']).lower()
                Contact    = f"{FirstName} {LastName}"
                Email      = nb_fe_contracts['contact']['email']
                Status     = (nb_fe_contracts['contractType']).lower()
                if 'platformId' not in nb_fe_contracts:
                    Id  = 'None'
                else:
                    Id  = nb_fe_contracts['platformId']
                
                ### print()
    else:
        print(r.status_code)
```

### Get monthly consumption from FE (the last bill)
```python
def get_fe_invoices(fe_token_url,fe_data,fe_bills_url,fe_headers,fe_cloudstore_token,fe_contract_id,fe_month,BillingPeriod)
    print('Please wait a moment...(this can again take several minutes)')

    fe_month=str(fe_month)
    r = requests.post(fe_token_url, data=fe_data, headers=fe_headers)
    if r.status_code == 200:
        token = r.json()
        fe_orange_token = token['access_token']
    fe_headers  = {"Accept": "application/json","Authorization": f"Bearer {fe_orange_token}","X-API-Key": f"{fe_cloudstore_token}","X-ECCS-Contract-Id": f"{fe_contract_id}"}
    r = requests.get(fe_bills_url, headers=fe_headers)
    if r.status_code == 200:
        bills_request = r.json()
        for value in bills_request:
            if fe_month in value['period']:
                billsId = value['id']
                invoices_url = f"https://api.orange.com/cloud/b2b/v1/documents/{billsId}/file"
                r = requests.get(invoices_url, headers=fe_headers, allow_redirects=True)
                if r.status_code == 200:
                    open('fe.xlsx', 'wb').write(r.content)
                    workbook = load_workbook(filename = f"fe.xlsx")
                    sheet_ranges = workbook['Billing Report']
                    #date = sheet_ranges.cell(row=8, column=2)
                    #BillingPeriod = date.value[18:30].replace("/",'-')
                    sheet_ranges.delete_rows(0,13)

                    for x in sheet_ranges:
                        BillingMode = x[7].value
                        if BillingMode == "Usage":
                            Reference        = x[0].value
                            Service          = x[10].value
                            Category         = x[11].value
                            SubCategory      = '' #unavailable
                            Quantity         = x[12].value
                            Region           = x[9].value
                            Tag              = 'flexible engine'
                            Unit             = x[13].value
                            UnitPrice        = x[14].value
                            Amount           = x[15].value
                            DiscountedAmount = x[17].value
                            BillingPeriod    = (datetime.now()- timedelta(days=15)).strftime("%Y-%m-28")

                            ### print()
                else:
                    print(r.status_code)
    else:
        print(r.status_code)                   
```
