## Azure billing reports (with Tier2 Arrow)

### Authentication
You must create a key to call Arrow API : https://xsp.arrow.com/index.php/apidoc/keys \
The examples in this section use the (non-working) credentials in the following table.
<table>
  <tr>
    <th>Parameter</th>
    <th>Value</th>
  </tr>
  <tr>
    <td>KeyName</td>
    <td>ArrowEXAMPLE</td>
  </tr>
  <tr>
    <td>Key</td>
    <td>EYSEXvsjFcEXAMPLEKEY</td>
  </tr>
  <tr>
    <td>Secret</td>
    <td>SecretEXAMPLE</td>
  </tr>
</table>

### python script example
minimum version : Python 3.6.x
```python
# -*- coding: utf-8 -*-
import requests,json,os,urllib3,datetime
from datetime import datetime,date
from dateutil.relativedelta import relativedelta
urllib3.disable_warnings()
```

### VAULT authentication methods (without AppRole)
- Variables should be adapted for your specific context
```python
vault_addr    = "your_vault_secret_engine"
today         = date.today()
endDate       = date(today.year, today.month, 1)
startDate     = endDate + relativedelta(months=-1)
az_month      = startDate.strftime("%Y-%m")
BillingPeriod = startDate.strftime("%Y-%m-28")
```

```python
def get_vault_credentials(vault_addr,vault_headers):
    r = requests.get(vault_addr, headers=vault_headers, verify=False)
    if r.status_code == 200:
        vault_result = r.json()
        return vault_result
    else:
        print(r.status_code)

vault_login      = input("Please enter your cuid (AD-SUBS) : ")
vault_token      = os.popen(f"vault login -method=ldap -path=ad-subs username={vault_login}").read()
print(vault_token)
token            = input("Please enter your vault token : ")
vault_headers    = {"X-Vault-Token": f"{token}","Content-Type": "application/json"}
vault_addr       = "your_vault_secret_engine"
vault_result     = get_vault_credentials(vault_addr,vault_headers)
az_apikey        = vault_result['data']['apikey']
az_signature     = vault_result['data']['signature']
```

### Get arrow url + credentials
```python
az_customers_url = 'https://xsp.arrow.com/index.php/api/customers'
az_orders_url    = 'https://xsp.arrow.com/index.php/api/orders'
az_invoices_url  = 'https://xsp.arrow.com/index.php/api/consumption/license'
az_headers       = {'apikey': f'{az_apikey}','signature': f'{az_signature}','Content-Type': 'application/json'}
```

### Get Customers
```python
def get_csp_customers(az_customers_url,az_headers):
    print('Please wait a moment...(this can again take several minutes)')

    r = requests.get(az_customers_url, headers=az_headers)
    if r.status_code == 200:       
        nb_azure_customers = r.json()
        for azure_customers in nb_azure_customers['data']['customers']:
            Reference   = azure_customers['Reference']
            TenantName  = azure_customers['CompanyName']
            Id          = azure_customers['InternalReference']
            LastName    = (azure_customers['Contact']['LastName']).upper()
            FirstName   = (azure_customers['Contact']['FirstName']).lower()
            Contact     = f"{FirstName} {LastName}"
            Email       = azure_customers['Contact']['Email']
            Tag         = 'azure'

            orders = get_az_orders(az_orders_url,az_headers)
            for value in orders.values():
                Status       = value['Status']
                Created      = value['Created']

                ### print()
    else:
        print(r.status_code)
```

### Get monthly consumption from Arrow (the last bill)
```python
def get_azure_invoices(az_orders_url,az_invoices_url,az_headers,az_month,BillingPeriod):
    print('Please wait a moment...(this can again take several minutes)')

    az_month=str(az_month)
    r = requests.get(az_orders_url, headers=az_headers)
    if r.status_code == 200:  
        nb_azure_orders = r.json()
        for azure_orders in nb_azure_orders['data']['orders']:
            Reference = azure_orders['customer']['reference']
            License   = azure_orders['products'][0]['license']['reference']

            invoices_url = az_invoices_url+'/'+License+'?month='+az_month+'&columns[0]=Vendor Ressource SKU&columns[1]=Vendor Product Name&columns[2]=Vendor Meter Category&columns[3]=Vendor Meter Sub-Category&columns[4]=Resource Group&columns[5]=UOM&columns[6]=Country currency code&columns[7]=Level Chargeable Quantity&columns[8]=Region&columns[9]=Resource Name&columns[10]=Country customer unit&columns[11]=Vendor Billing Start Date&columns[12]=Vendor Billing End Date&columns[13]=Cost Center&columns[14]=Project&columns[15]=Environment&columns[16]=Application&columns[17]=Custom Tag&columns[18]=Name&columns[19]=Usage Start date'        
            r = requests.get(invoices_url, headers=az_headers)
            if r.status_code == 200:
                az_invoices = r.json()
                for value in az_invoices['data']['lines']:
                    if value != []:
                        Service          = value[2]
                        Category         = value[3]
                        SubCategory      = value[1]
                        Quantity         = value[10]
                        Region           = value[8]
                        Tag              = 'azure'
                        Unit             = value[5]
                        UnitPrice        = value[7]
                        Amount           = (Quantity*UnitPrice)
                        DiscountedAmount = Amount

                        ##print()
            else:
                print(r.status_code)
    else:
        print(r.status_code)
```
