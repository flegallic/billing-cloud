## Azure billing reports

### Different ways of buying Azure Services (Pay-As-You-Go, EA, Direct CSP, Indirect CSP)
Here is a diagram that illustrates the relationships in this model : \
![](azure-ways.png)
 
### Direct CSP Reseller (Partner Center)
Larger and more mature partners will often opt to become a Direct CSP Reseller. This means that the partner deals directly with Microsoft. \
Here is a diagram that explains the relationships in this model

### Indirect CSP Reseller (Tier2)
For partners just starting out selling CSP, becoming an Indirect Reseller is usually the most sensible option. The requirements for Indirect Resellers are relatively simple. \
Indirect Resellers establish a relationship with an Indirect Provider or Distributor. The Distributor maintains the relationship with Microsoft. The Indirect Reseller maintains the relationship with the Distributor. Because there is a Distributor between the Reseller and Microsoft, they use the term “Indirect Reseller”.