## Azure billing reports (shared services only)

### Authentification
- Set up API access in Partner Center : https://docs.microsoft.com/en-us/partner-center/develop/set-up-api-access-in-partner-center

- Web Application : \
You must create a web app to call Partner Center REST API :  https://partner.microsoft.com/fr-fr/pcv/apiintegration/appmanagement \
The examples in this section use the (non-working) credentials in the following table.
<table>
  <tr>
    <th>Parameter</th>
    <th>Value</th>
  </tr>
  <tr>
    <td>tenantId</td>
    <td>xxxxxxxxxEXAMPLE</td>
  </tr>
  <tr>
    <td>appId</td>
    <td>xxxxxxxxxEXAMPLE</td>
  </tr>
  <tr>
    <td>appSecret</td>
    <td>SecretEXAMPLE</td>
  </tr>
  <tr>
    <td>sharedTenantId</td>
    <td>xxxxxxxxxEXAMPLE</td>
  </tr>
</table>

### python script example
minimum version : Python 3.6.x
```python
# -*- coding: utf-8 -*-
import requests,json,os,urllib3,datetime
from datetime import datetime,date
from dateutil.relativedelta import relativedelta
urllib3.disable_warnings()
```

### VAULT authentication methods (without AppRole)
- Variables should be adapted for your specific context
```python
vault_addr    = "your_vault_secret_engine"
today         = date.today()
endDate       = date(today.year, today.month, 1)
startDate     = endDate + relativedelta(months=-1)
apss_month    = startDate.strftime("%Y-%m-27T00:00:00Z")
BillingPeriod = startDate.strftime("%Y-%m-28")
```

```python
def get_vault_credentials(vault_addr,vault_headers):
    r = requests.get(vault_addr, headers=vault_headers, verify=False)
    if r.status_code == 200:
        vault_result = r.json()
        return vault_result
    else:
        print(r.status_code)
        
vault_login         = input("Please enter your cuid (AD-SUBS) : ")
vault_token         = os.popen(f"vault login -method=ldap -path=ad-subs username={vault_login}").read()
print(vault_token)
token               = input("Please enter your vault token : ")
vault_headers       = {"X-Vault-Token": f"{token}","Content-Type": "application/json"}
vault_addr          = "your_vault_secret_engine"
vault_result        = get_vault_credentials(vault_addr,vault_headers)
apss_tenantId       = vault_result['data']['tenantId']
apss_appId          = vault_result['data']['appId']
apss_appSecret      = vault_result['data']['appSecret']
apss_sharedTenantId = vault_result['data']['sharedTenantId']
```

### Get apss url + credentials
```python
apss_token_url         = f'https://login.microsoftonline.com/{apss_tenantId}/oauth2/token'
apss_data              = f'grant_type=client_credentials&client_id={apss_appId}&client_secret={apss_appSecret}&resource=https://graph.windows.net'
apss_subscriptions_url = f'https://api.partnercenter.microsoft.com/v1/customers/{apss_sharedTenantId}/subscriptions'
apss_customers_url     = f'https://api.partnercenter.microsoft.com/v1/customers/{apss_sharedTenantId}'
apss_r                 = requests.post(apss_token_url, data=apss_data)          
apss_token_request     = apss_r.json()
apss_token             = apss_token_request['access_token']
apss_headers           = {"Authorization": f"Bearer {apss_token}","Accept": "application/json"}
```

### Get Customers
```python
def get_csp_customers(apss_customers_url,apss_subscriptions_url,apss_headers,apss_sharedTenantId):
    print('Please wait a moment...(this can again take several minutes)')

    r = requests.get(apss_subscriptions_url, headers=apss_headers)
    if r.status_code == 200:
        r.encoding = 'utf-8-sig'
        nb_apss_subscriptions = json.loads(r.text)
        for apss_subscriptions in nb_apss_subscriptions['items']:
            Reference  = apss_subscriptions['entitlementId']
            TenantName = apss_subscriptions['friendlyName']
            Status     = apss_subscriptions['status']
            if Status == 'active':
                Created    = apss_subscriptions['creationDate']
                Id         = apss_sharedTenantId
                Tag        = "apss"

                r = requests.get(apss_customers_url, headers=apss_headers)
                if r.status_code == 200:
                    r.encoding = 'utf-8-sig'
                    apss_customers = json.loads(r.text)
                    LastName   = (apss_customers['billingProfile']['lastName']).upper()
                    FirstName  = (apss_customers['billingProfile']['firstName']).lower()
                    Contact    = f"{FirstName} {LastName}"
                    Email      = apss_customers['billingProfile']['email']

                    ### print()         
                else:
                    print(r.status_code)
    else:
        print(r.status_code)
```

### Get monthly consumption from APSS (the last bill)
```python
def get_apss_invoices(apss_customers_url,apss_subscriptions_url,apss_headers,apss_sharedTenantId,apss_month,BillingPeriod):
    print('Please wait a moment...(this can again take several minutes)')

    apss_month=str(apss_month)
    invoices_url = f'https://api.partnercenter.microsoft.com/v1/invoices/'
    r = requests.get(invoices_url, headers=apss_headers)
    if r.status_code == 200:
        r.encoding='utf-8-sig'
        data = json.loads(r.text)
        for invoices in data['items']:
            billingId = invoices['id'] ## billingId == internal contract
            billingPeriodEndDate = invoices['billingPeriodEndDate']
            if billingPeriodEndDate == apss_month:
                invoices_url = f'https://api.partnercenter.microsoft.com/v1/invoices/Recurring-{billingId}/lineitems/Azure/BillingLineItems'
                r = requests.get(invoices_url, headers=apss_headers)
                r.encoding='utf-8-sig'
                data = json.loads(r.text)
                for value in data['items']:                  
                    postTaxEffectiveRate = value['postTaxEffectiveRate']
                    Reference            = value['subscriptionId']
                    Service              = value['serviceName']
                    Category             = value['serviceType']
                    SubCategory          = value['resourceName']
                    Quantity             = value['consumedQuantity']
                    Region               = value['region']
                    Tag                  = 'apss'
                    Unit                 = value['unit']
                    Unit                 = value['unit']
                    if postTaxEffectiveRate == 0 or Quantity == 0:
                        UnitPrice        = '0'
                    else:
                        UnitPrice        = Quantity//postTaxEffectiveRate
                    Amount               = value['postTaxTotal']
                    DiscountedAmount     = Amount
                    BillingPeriod        = (datetime.now()- timedelta(days=15)).strftime("%Y-%m-28")

                    ### print()
```
