# billing-cloud ![](logo.png)

## Overview
This repository contains the script billing-cloud, used to pull to cost and usage data for your Azure, AWS, Flexible Engine resources with REST APIs. \
A simple script that calls the billing usage REST API to retrieve resource usage data for a subscription. \
Each provider offers a consumption data details. The API includes:

<table>
  <tr>
    <th>Cloud provider</th>
    <th>Type</th>
    <th>API</th>
    <th>Customers</th>
    <th>Invoices</th>
    <th>Monthly consumptions</th>
    <th>Usage details</th>
  </tr>
  <tr>
    <td>AWS</td>
    <td>Direct</td>
    <td>Amazon</td>
    <td>:heavy_check_mark:</td>
    <td>:heavy_check_mark:</td>
    <td>:heavy_check_mark:</td>
    <td>:heavy_check_mark:</td>
  </tr>
  <tr>
    <td>Flexible Engine</td>
    <td>Direct</td>
    <td>Cloud Store</td>
    <td>:heavy_check_mark:</td>
    <td>:heavy_check_mark: (csv only)</td>
    <td>:heavy_check_mark: (csv only)</td>
    <td>:x:</td>
  </tr>
  <tr>
    <td>Azure</td>
    <td>Indirect (with Tier2)</td>
    <td>Arrow</td>
    <td>:heavy_check_mark:</td>
    <td>:heavy_check_mark:</td>
    <td>:heavy_check_mark:</td>
    <td>:heavy_check_mark:</td>
  </tr>
  <tr>
    <td>APSS</td>
    <td>Direct</td>
    <td>Azure Partner Center</td>
    <td>:heavy_check_mark:</td>
    <td>:heavy_check_mark:</td>
    <td>:heavy_check_mark:</td>
    <td>:heavy_check_mark:</td>
  </tr>
  <tr>
    <td>GCP</td>
    <td>Direct</td>
    <td>Google</td>
    <td>:heavy_check_mark:</td>
    <td>:heavy_check_mark:</td>
    <td>:heavy_check_mark:</td>
    <td>:heavy_check_mark:</td>
  </tr>
</table>

## Documentation
<table>
  <tr>
    <th>Cloud provider</th>
    <th>Prerequisites</th>
    <th>Url</th>
  </tr>
  <tr>
    <td>Amazon</td>
    <td>Open source</td>
    <td>https://docs.aws.amazon.com/aws-cost-management/latest/APIReference/API_GetCostAndUsage.html<br>
    https://boto3.amazonaws.com/v1/documentation/api/latest/reference/services/ce.html</td>
  </tr>
  <tr>
    <td>Flexible Engine</td>
    <td>Developer account</td>
    <td>https://developer.orange.com/apis/cloudstorecustomerspace/getting-started</td>
  <tr> 
    <td>APSS (Partner Center)</td>
    <td>Open source</td>
    <td>https://docs.microsoft.com/fr-fr/partner-center/develop/partner-center-rest-api-reference</td>
  </tr>
  <tr>
    <td>Azure (with Arrow) </td>
    <td>Developer account</td>
    <td>https://xsp.arrow.com/apidoc/</td>
  </tr>
  <tr>
    <td>GCP</td>
    <td>Developer account</td>
    <td>https://developers.google.com/fit/rest/v1/get-started
    <br>https://googleapis.dev/python/cloudbilling/latest/index.html</td>
  </tr>
</table>