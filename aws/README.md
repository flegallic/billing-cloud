## AWS billing reports

### Authentication
You must login to call Amazon API gateway. \
The examples in this section use the (non-working) credentials in the following table.
<table>
  <tr>
    <th>Parameter</th>
    <th>Value</th>
  </tr>
  <tr>
    <td>aws_access_key_id</td>
    <td>AKIAIOSFODNN7_EXAMPLE</td>
  </tr>
  <tr>
    <td>aws_secret_access_key</td>
    <td>wJalrXUtnFEMI/K7MDENG/bPxRfiCY_EXAMPLE</td>
  </tr>
  <tr>
    <td>region_name</td>
    <td>us-east-xxx</td>
  </tr>
  <tr>
    <td>RoleArn</td>
    <td>arn:aws:iam::account-of-role-to-assume:role/name-of-role</td>
  </tr>
  <tr>
    <td>RoleSessionName</td>
    <td>AssumeRoleSessionName</td>
  </tr>
</table>

### python script example
minimum version : Python 3.6.x
```python
# -*- coding: utf-8 -*-
import requests,json,os,urllib3,datetime,boto3
from datetime import datetime,date
from dateutil.relativedelta import relativedelta
urllib3.disable_warnings()
```

### VAULT authentication methods (without AppRole)
- Variables should be adapted for your specific context
```python
vault_addr = "your_vault_secret_engine"
today         = date.today()
endDate       = date(today.year, today.month, 1)
startDate     = endDate + relativedelta(months=-1)
BillingPeriod = startDate.strftime("%Y-%m-28")
```

```python
def get_vault_credentials(vault_addr,vault_headers):
    r = requests.get(vault_addr, headers=vault_headers, verify=False)
    if r.status_code == 200:
        vault_result = r.json()
        return vault_result
    else:
        print(r.status_code)
        
vault_login           = input("Please enter your cuid (AD-SUBS) : ")
vault_token           = os.popen(f"vault login -method=ldap -path=ad-subs username={vault_login}").read()
print(vault_token)
token                 = input("Please enter your vault token : ")
vault_headers         = {"X-Vault-Token": f"{token}","Content-Type": "application/json"}
vault_addr            = "your_vault_secret_engine"
vault_result          = get_vault_credentials(vault_addr,vault_headers)
aws_access_key_id     = vault_result['data']['aws_access_key_id']
aws_secret_access_key = vault_result['data']['aws_secret_access_key']
aws_region_name       = vault_result['data']['aws_region_name']
aws_roleArn           = vault_result['data']['aws_roleArn']
aws_roleSessionName   = vault_result['data']['aws_roleSessionName']
```

### Get Customers
```python
def get_csp_customers(aws_access_key_id,aws_secret_access_key,aws_region_name):
    print('Please wait a moment...(this can again take several minutes)')

    customers_list = {}
    boto3session = boto3.Session(region_name=f'{aws_region_name}',aws_access_key_id=f'{aws_access_key_id}',aws_secret_access_key=f'{aws_secret_access_key}')
    clientorga = boto3session.client('organizations')
    ###customers without token
    customers_request  = clientorga.list_accounts()
    aws_customers_list = customers_request["Accounts"]
    for customers in aws_customers_list:
        Reference  = customers['Id'] 
        TenantName = customers['Name']   
        Status     = customers['Status']
        Email      = customers['Email']
        Created    = customers['JoinedTimestamp']
        Tag        = 'aws'
        Contact    = ''
        customers_list[Reference] = {'TenantName':TenantName,'Reference':Reference,'Contact':Contact,'Email':Email,'Tag':Tag,'Created':Created,'Status':Status}
    ###customers with token (if exist)
    if customers_request.get("NextToken") is not None:
        customers_request  = clientorga.list_accounts()
        aws_token = customers_request["NextToken"]
        customers_request  = clientorga.list_accounts(NextToken=aws_token)
        aws_customers_list = customers_request["Accounts"]
        for customers in aws_customers_list:      
            Reference  = customers['Id'] 
            TenantName = customers['Name']   
            Status     = customers['Status']
            Email      = customers['Email']
            Created    = customers['JoinedTimestamp']
            Tag        = 'aws'
            Contact    = '' #unavailable
            customers_list[Reference] = {'TenantName':TenantName,'Reference':Reference,'Contact':Contact,'Email':Email,'Tag':Tag,'Created':Created,'Status':Status}
        for aws_customers in customers_list.values():
            Reference   = aws_customers['Reference']
            TenantName  = aws_customers['TenantName']
            Contact     = aws_customers['Contact']
            Email       = aws_customers['Email']
            Tag         = aws_customers['Tag']
            Status      = (aws_customers['Status']).lower()
            Created     = aws_customers['Created']
            Id          = '' #unavailable

            ### print()
```

### Get Customers with RoleArn(IAM)
```python
def get_csp_customers(aws_access_key_id,aws_secret_access_key,aws_region_name,aws_roleArn,aws_roleSessionName):
    print('Please wait a moment...(this can again take several minutes)')

    sesh = boto3.session.Session(
        aws_access_key_id=f'{aws_access_key_id}',
        aws_secret_access_key=f'{aws_secret_access_key}',
        region_name=f'{aws_region_name}'  
    )
    ##Create an STS client object that represents a live connection
    sts_client = sesh.client('sts')
    ##Call the assume_role method of the STSConnection object and pass the role
    assumed_role_object = sts_client.assume_role(
        RoleArn = f'{aws_roleArn}',
        RoleSessionName=f'{aws_roleSessionName}'
    )
    ##Get the temporary credentials that can be used to make subsequent API calls
    credentials = assumed_role_object['Credentials']
    clientorga = boto3.client(
        'organizations',
        aws_access_key_id = credentials['AccessKeyId'],
        aws_secret_access_key = credentials['SecretAccessKey'],
        aws_session_token = credentials['SessionToken']
    )   
    ##AssumeRole returns to make a connection
    customers_request  = clientorga.list_accounts()
    
    ###customers without token
    aws_customers_list = customers_request["Accounts"]
    for customers in aws_customers_list:
        Reference  = customers['Id'] 
        TenantName = customers['Name']   
        Status     = customers['Status']
        Email      = customers['Email']
        Created    = customers['JoinedTimestamp']
        Tag        = 'aws'
        Contact    = ''
        customers_list[Reference] = {'TenantName':TenantName,'Reference':Reference,'Contact':Contact,'Email':Email,'Tag':Tag,'Created':Created,'Status':Status}
    ###customers with token (if exist)
    if customers_request.get("NextToken") is not None:
        customers_request  = clientorga.list_accounts()
        aws_token = customers_request["NextToken"]
        customers_request  = clientorga.list_accounts(NextToken=aws_token)
        aws_customers_list = customers_request["Accounts"]
        for customers in aws_customers_list:      
            Reference  = customers['Id'] 
            TenantName = customers['Name']   
            Status     = customers['Status']
            Email      = customers['Email']
            Created    = customers['JoinedTimestamp']
            Tag        = 'aws'
            Contact    = '' #unavailable
            customers_list[Reference] = {'TenantName':TenantName,'Reference':Reference,'Contact':Contact,'Email':Email,'Tag':Tag,'Created':Created,'Status':Status}
        for aws_customers in customers_list.values():
            Reference   = aws_customers['Reference']
            TenantName  = aws_customers['TenantName']
            Contact     = aws_customers['Contact']
            Email       = aws_customers['Email']
            Tag         = aws_customers['Tag']
            Status      = (aws_customers['Status']).lower()
            Created     = aws_customers['Created']
            Id          = '' #unavailable

            ### print()
```

### Get monthly consumption from AWS (the last bill)
```python
def get_aws_invoices(aws_access_key_id,aws_secret_access_key,aws_region_name,startDate,endDate,BillingPeriod)
    print('Please wait a moment...(this can again take several minutes)')

    startDate=str(startDate)
    endDate=str(endDate)
    boto3session = boto3.Session(region_name=f'{aws_region_name}',aws_access_key_id=f'{aws_access_key_id}',aws_secret_access_key=f'{aws_secret_access_key}')
    clientorga = boto3session.client('organizations')
    clientcostexplorer = boto3session.client('ce')
    invoices_list = {}
    ##request by service
    cost_request = clientcostexplorer.get_cost_and_usage(
    TimePeriod={
        'Start': startDate,
        'End': endDate
    },
    Granularity='MONTHLY',
    Metrics=['BlendedCost','UnblendedCost','UsageQuantity'],
    GroupBy=[
        {
            'Type': 'DIMENSION',
            'Key': 'USAGE_TYPE'
        },
        {
            'Type': 'DIMENSION',
            'Key': 'SERVICE'
        }        
    ]
    )
    i = 0
    for value in cost_request['ResultsByTime'][0]['Groups']:
        i = i +1
        Service       = value['Keys'][1]
        Category = value['Keys'][0]

        invoices_list[Category] = {'Category':Category,'Service':Service}

    ##request by usage
    cost_request = clientcostexplorer.get_cost_and_usage(
    TimePeriod={
        'Start': startDate,
        'End': endDate
    },
    Granularity='MONTHLY',
    Metrics=['BlendedCost','UnblendedCost','UsageQuantity'],
    GroupBy=[
        {
            'Type': 'DIMENSION',
            'Key': 'USAGE_TYPE'
        },
        {
            'Type': 'DIMENSION',
            'Key': 'LINKED_ACCOUNT'
        }        
    ]
    )
    for value in cost_request['ResultsByTime'][0]['Groups']:
        Reference        = value['Keys'][1]
        Category         = value['Keys'][0]
        SubCategory      = 'N/A'
        Quantity         = value['Metrics']['UsageQuantity']['Amount']
        Region           = aws_region_name
        Unit             = value['Metrics']['UsageQuantity']['Unit']
        UnitPrice        = 'N/A'
        Amount           = value['Metrics']['UnblendedCost']['Amount']
        DiscountedAmount = value['Metrics']['BlendedCost']['Amount']

        for value in invoices_list.values():
            usageCategory   = value['Category']
            Service    = value['Service']
            if usageCategory == Category:
                ### print()
```

### Get monthly consumption from AWS with RoleArn(IAM)
```python
def get_aws_invoices(aws_access_key_id,aws_secret_access_key,aws_region_name,aws_roleArn,aws_roleSessionName,startDate,endDate,BillingPeriod)
    print('Please wait a moment...(this can again take several minutes)')

    startDate=str(startDate)
    endDate=str(endDate)

    sesh = boto3.session.Session(
        aws_access_key_id=f'{aws_access_key_id}',
        aws_secret_access_key=f'{aws_secret_access_key}',
        region_name=f'{aws_region_name}'  
    ) 
    ##Create an STS client object that represents a live connection
    sts_client = sesh.client('sts')
    ##Call the assume_role method of the STSConnection object and pass the role
    assumed_role_object = sts_client.assume_role(
        RoleArn = f'{aws_roleArn}',
        RoleSessionName=f'{aws_roleSessionName}'
    )
    ##Get the temporary credentials that can be used to make subsequent API calls
    credentials = assumed_role_object['Credentials']
    clientorga = boto3.client(
        'organizations',
        aws_access_key_id = credentials['AccessKeyId'],
        aws_secret_access_key = credentials['SecretAccessKey'],
        aws_session_token = credentials['SessionToken']
    )

    invoices_list = {}
    ##AssumeRole returns to make a connection
    cost_request = clientorga.get_cost_and_usage(
    TimePeriod={
        'Start': startDate,
        'End': endDate
    },
    Granularity='MONTHLY',
    Metrics=['BlendedCost','UnblendedCost','UsageQuantity'],
    GroupBy=[
        {
            'Type': 'DIMENSION',
            'Key': 'USAGE_TYPE'
        },
        {
            'Type': 'DIMENSION',
            'Key': 'SERVICE'
        }        
    ]
    )
    i = 0
    for value in cost_request['ResultsByTime'][0]['Groups']:
        i = i +1
        Service       = value['Keys'][1]
        Category = value['Keys'][0]

        invoices_list[Category] = {'Category':Category,'Service':Service}

    ##request by usage
    cost_request = clientcostexplorer.get_cost_and_usage(
    TimePeriod={
        'Start': startDate,
        'End': endDate
    },
    Granularity='MONTHLY',
    Metrics=['BlendedCost','UnblendedCost','UsageQuantity'],
    GroupBy=[
        {
            'Type': 'DIMENSION',
            'Key': 'USAGE_TYPE'
        },
        {
            'Type': 'DIMENSION',
            'Key': 'LINKED_ACCOUNT'
        }        
    ]
    )
    for value in cost_request['ResultsByTime'][0]['Groups']:
        Reference        = value['Keys'][1]
        Category         = value['Keys'][0]
        SubCategory      = 'N/A'
        Quantity         = value['Metrics']['UsageQuantity']['Amount']
        Region           = aws_region_name
        Unit             = value['Metrics']['UsageQuantity']['Unit']
        UnitPrice        = 'N/A'
        Amount           = value['Metrics']['UnblendedCost']['Amount']
        DiscountedAmount = value['Metrics']['BlendedCost']['Amount']

        for value in invoices_list.values():
            usageCategory   = value['Category']
            Service    = value['Service']
            if usageCategory == Category:
                ### print()
```